package lajthabalazs.table;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;

/**
 * Feladat: Egy néhány oldalas táblázat első oldalán a fejléc alatt maxFirst sor fér el. A többi oldalon max sor fér el.
 * A táblázat után egy záró elem szerepel majd, ami footer sornyi helyet foglal.
 * A kezdőoldal kivételével egyik oldalon sem lehet kevesebb, mint min sor, a záró elem nem szerepelhet egyedül az utolsó oldalon.
 * Az utolsó oldal kivételével minden oldalon a szabályok által megengedett lehető legtöbb sor legyen.
 * 
 * Írj metódust, ami paraméterként kapja a maxFirst, max , footer és min értékeket, valamint a táblázat sorainak számát,
 * és egy int listát ad vissza, ami azt tartalmazza, hogy melyik oldalra hány sor kell kerüljön.
 *
 */
public class TableRowRendererTest {
	/* */
	private TableRenderer renderer;

	@Before
	public void init() {
		renderer = new TableRenderer();
	}

	/* */
	@Test
	public void singleRowOnFirstPage() {
		List<Integer> pages = renderer.render(
				maxFirstPage(12),
				maxOnPage(16),
				footerHeight(4),
				minOnLastPage(3),
				rowCount(1));
		assertThat("Pages: " + pages, pages, contains(1));
	}

	/* */
	@Test
	public void singlePageWithSevenItems() {
		List<Integer> pages = renderer.render(
				maxFirstPage(12),
				maxOnPage(16),
				footerHeight(4),
				minOnLastPage(3),
				rowCount(7));
		assertThat("Pages: " + pages, pages, contains(7));
	}

	/* */
	@Test
	public void renderOnTwoPages() {
		List<Integer> pages = renderer.render(
				maxFirstPage(12),
				maxOnPage(16),
				footerHeight(4),
				minOnLastPage(3),
				rowCount(20));
		assertThat("Pages: " + pages, pages, contains(12, 8));
	}

	/* */
	@Test
	public void renderOnThreePages() {
		List<Integer> pages = renderer.render(
				maxFirstPage(12),
				maxOnPage(16),
				footerHeight(4),
				minOnLastPage(3),
				rowCount(32));
		assertThat("Pages: " + pages, pages, contains(12, 16, 4));
	}

	/* */
	@Test
	public void doesntRenderLessThenMinOnPage() {
		List<Integer> pages = renderer.render(
				maxFirstPage(12),
				maxOnPage(16),
				footerHeight(4),
				minOnLastPage(3),
				rowCount(29));
		assertThat("Pages: " + pages, pages, contains(12, 14, 3));
	}

	/* */
	@Test
	public void doesntLeaveToMuchForLastPageIfFooterWouldBeNeeded() {
		List<Integer> pages = renderer.render(
				maxFirstPage(12),
				maxOnPage(16),
				footerHeight(4),
				minOnLastPage(3),
				rowCount(28));
		assertThat("Pages: " + pages, pages, contains(12, 13, 3));
	}

	/* */
	@Test
	public void testWithOtherNumbers() {
		List<Integer> pages = renderer.render(
				maxFirstPage(17),
				maxOnPage(21),
				footerHeight(7),
				minOnLastPage(2),
				rowCount(79));
		assertThat("Pages: " + pages, pages, contains(17, 21, 21, 18, 2));
	}
	/* */

	private static final int footerHeight(int i) {
		return i;
	}

	private static final int maxFirstPage(int i) {
		return i;
	}

	private static final int maxOnPage(int i) {
		return i;
	}

	private static final int rowCount(int i) {
		return i;
	}

	private static final int minOnLastPage(int i) {
		return i;
	}
}