package lajthabalazs.table;

import java.util.ArrayList;
import java.util.List;

public class TableRenderer {

	public List<Integer> render(int maxFirstPage, int maxOnPage, int footerHeight, int minOnLastPage, int rowCount) {
		List<Integer> pages = new ArrayList<>();
		if (rowCount == 0) {
			return pages;
		}
		int onPage = Math.min(maxFirstPage, rowCount);
		onPage = adjustForFooter(maxFirstPage, footerHeight, rowCount, onPage);
		onPage = adjustForMinOnPage(minOnLastPage, rowCount, onPage);
		pages.add(onPage);
		pages.addAll(render(maxOnPage, maxOnPage, footerHeight, minOnLastPage, rowCount - onPage));
		return pages;
	}

	private int adjustForFooter(int maxFirstPage, int footerHeight, int rowCount, int onPage) {
		if(onPage == rowCount) {
			if (maxFirstPage < onPage + footerHeight) {
				onPage = onPage - 1;
			}
		}
		return onPage;
	}

	private int adjustForMinOnPage(int minOnLastPage, int rowCount, int onPage) {
		if (rowCount - onPage > 0 && rowCount - onPage < minOnLastPage) {
			onPage = rowCount - minOnLastPage;
		}
		return onPage;
	}
}